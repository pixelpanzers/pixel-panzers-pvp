﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAnimations : MonoBehaviour
{
    const float AnimationSpeed = 1;
    const float MaxFade = 1.4f;

    private static Color White = Color.white;
    private static Color Transparent = new Color(1, 1, 1, 0);

    private Animator Driving;
    private Animator DustForward;
    private Animator DustBackward;
    private Transform SmokePoint;
    private Transform DustPoint;
    private Animator Gun1;
    private Animator Gun2;

    void Start ()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);

            if (child.name.Contains("gun"))
                Gun1 = child.GetComponent<Animator>();
            else if (child.name.Contains("gun2"))
                Gun2 = child.GetComponent<Animator>();
            else if (child.name == ("driving"))
                Driving = child.GetComponent<Animator>();
            else if (child.name.Contains("dirt_animation_F"))
                DustForward = child.GetComponent<Animator>();
            else if (child.name.Contains("dirt_animation_B"))
                DustBackward = child.GetComponent<Animator>();
            else if (child.name.Contains("firing_dust"))
                DustPoint = child;
            else if (child.name.Contains("Smoke"))
                SmokePoint = child.transform;
        }

        if(DustForward != null)
            DustForward.GetComponent<Renderer>().material.color = Transparent;
        if(DustBackward != null)
            DustBackward.GetComponent<Renderer>().material.color = Transparent;
    }

    public void DrawDamage(string _damage, Tank.DamageType _type, Vector2 _pos)
    {
        float NumWidth = 0.08f;
        float CurSpawnX = _pos.x - (_damage.Length / 2 * NumWidth);

        foreach (char c in _damage)
        {
            GameObject Prefab;

            //  Ignore non-numerical values
            //  Ignore none damage values
            if (!char.IsNumber(c) || _type == Tank.DamageType.NONE)
                continue;

            //  Load the correct prefab for the damage type
            if (_type != Tank.DamageType.PIERCING)
                Prefab = (GameObject)Resources.Load("Number_" + c);
            else
                Prefab = (GameObject)Resources.Load("Number_" + c + "_P");

            //  Instantiate the loaded prefab
            GameObject Num = Instantiate(Prefab, new Vector2(CurSpawnX, _pos.y), Quaternion.identity);

            CurSpawnX += NumWidth;
        }
    }

    public void SetDrivingAnim(float _speed = 1)
    {
        if (_speed != 0)
            Driving.enabled = true;
        else
            Driving.enabled = false;

        Driving.speed = Mathf.Clamp01(_speed * AnimationSpeed * 2);
    }

    public void SetDustBackward(float _perc)
    {
        Color col = DustBackward.GetComponent<Renderer>().material.color;

        //  Don't let alpha change more than the max fade var
        float a = Mathf.Clamp(_perc,
            col.a - (Time.deltaTime * MaxFade),
            col.a + (Time.deltaTime * MaxFade));

        //  Slow the animation speed at slower speeds, but not /too/ slow
        DustBackward.speed = Mathf.Clamp01(_perc * 5) * AnimationSpeed;

        //  Set the alpha of the sprite based on the speed of the tank, but don't let it be /too/ transparent
        DustBackward.GetComponent<Renderer>().material.color = new Color(col.r, col.g, col.b, Mathf.Clamp01(a));
    }

    public void SetDustForward(float _perc)
    {
        Color col = DustForward.GetComponent<Renderer>().material.color;
        //  Don't let alpha change more than the max fade var
        float a = Mathf.Clamp(_perc,
            col.a - (Time.deltaTime * MaxFade),
            col.a + (Time.deltaTime * MaxFade));

        //  Slow the animation speed at slower speeds, but not /too/ slow
        DustForward.speed = Mathf.Clamp01(_perc * 5) * AnimationSpeed;

        //  Set the alpha of the sprite based on the speed of the tank, but don't let it be /too/ transparent
        DustForward.GetComponent<Renderer>().material.color = new Color(col.r, col.g, col.b, Mathf.Clamp01(a));
    }

    public void PlayFiringDust(Tank.TankDir _dir)
    {
        //  Load the shot_smoke prefab from the FX resources folder
        string prefabName = "firing_dust";

        if (_dir == Tank.TankDir.LEFT_FACING)
            prefabName = "firing_dust_l";

        GameObject Dust = Instantiate(Resources.Load<GameObject>(prefabName));
        Dust.transform.position = DustPoint.transform.position;
        Dust.transform.localScale = DustPoint.transform.localScale;
    }

    public void PlayFiringSmoke()
    {
        //  Load the shot_smoke prefab from the FX resources folder
        GameObject Smoke = Instantiate(Resources.Load<GameObject>("shot_smoke"));
        Smoke.transform.position = SmokePoint.transform.position;
        Smoke.transform.localScale = SmokePoint.transform.localScale;
    }

    public void PlayGun1Anim()
    {
        Gun1.enabled = true;
        Gun1.Play("entry", 0, 0);
    }
}