﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class DebugMove : MonoBehaviour
{
    [Range(-1f, 1f)]
    public float XSpeed = 0;
    [Range(-1f, 1f)]
    public float YSpeed = 0;

    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(XSpeed, YSpeed);
    }
}
