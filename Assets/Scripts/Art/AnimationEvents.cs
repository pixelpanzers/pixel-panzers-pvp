﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvents : MonoBehaviour
{
    private static Color White = Color.white;
    private static Color Transparent = new Color(1, 1, 1, 0);

    public void SetTransparency(float _perc)
    {
        GetComponent<SpriteRenderer>().material.color = new Color(1, 1, 1, _perc);
    }

    public void DisableObject()
    {
        this.gameObject.SetActive(false);
    }

    public void ResetAnimation(string _mecanimName)
    {
        GetComponent<Animator>().Play(_mecanimName, 0, 0);
        GetComponent<Animator>().enabled = false;
    }

    public void DeleteObject()
    {
        Destroy(gameObject);
    }

    public void DeleteParent()
    {
        Destroy(transform.parent.gameObject);
    }
}
