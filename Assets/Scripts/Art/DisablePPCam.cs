﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class DisablePPCam : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<PixelPerfectCamera>().enabled = false;
    }
}
