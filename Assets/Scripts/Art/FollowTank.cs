﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class FollowTank : MonoBehaviour
{
    public void LateUpdate()
    {
        if (!Tank.ActivePlayerTank)
            return;

        Vector3 TPos = Tank.ActivePlayerTank.transform.position.RoundPos();
        transform.position = TPos;
    }
}
