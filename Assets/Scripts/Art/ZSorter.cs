﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ZSorter : MonoBehaviour
{
    private float XWeight = 0.045f;
    public int Offset = 0;

    void LateUpdate()
    {
        SpriteRenderer Rend = GetComponent<SpriteRenderer>();

        Rend.sortingOrder = (int)(Offset - Camera.main.WorldToScreenPoint(Rend.bounds.min).y - 
            (Camera.main.WorldToScreenPoint(Rend.bounds.max).x * XWeight)) * -1;
    }
}
