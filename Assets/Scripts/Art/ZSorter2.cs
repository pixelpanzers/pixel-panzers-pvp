﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum PivotPositions
{
    BOTTOM_LEFT,
    BOTTOM_CENTER,
    CENTER_LEFT,
    TOP_RIGHT,
    CENTER
}

public enum PivotBase
{
    COLLIDER,
    SPRITE
}

[ExecuteInEditMode]
public class ZSorter2 : MonoBehaviour
{
    public PivotPositions Pivot = PivotPositions.CENTER;
    public PivotBase Base = PivotBase.COLLIDER;

    [Range(-5f, 5f)]
    public float XOffset = 0f;

    [Range(-5f, 5f)]
    public float YOffset = 0f;

    [Range(-2500f, 2500f)]
    public int Offset = 0;

    public GameObject CopyObject;

    public bool Kinematic = false;

    [Range(-1f, 1f)]
    public float XWeight = 0.045f;

    [Range(-1f, 1f)]
    public float YWeight = -0.174f;

    SpriteRenderer Rend;
    Collider2D Col;

    private void Awake()
    {
        Rend = GetComponent<SpriteRenderer>();
        Col = GetComponent<Collider2D>();
        SetZ();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(GetPivotPos(), 0.01f);
        SetZ();
    }

    void LateUpdate()
    {
        if (!Kinematic)
            SetZ();
    }

    void SetZ()
    {
        int z = 0;
        Vector2 PivotPos = GetPivotPos();

        if (CopyObject != null)
        {
            z = CopyObject.GetComponent<SpriteRenderer>().sortingOrder;
            z += Offset;
            Rend.sortingOrder = z;
        }
        else
        {
            z = (int)((-PivotPos.y) * 100f * /*ZSortValues.Instance.*/-YWeight);
            z += (int)((PivotPos.x * 100f) * /*ZSortValues.Instance.*/XWeight);

            z += Offset;

            Rend.sortingOrder = z;
        }
    }

    Vector2 GetPivotPos()
    {
        float xPos = 0;
        float yPos = 0;

        if (Pivot == PivotPositions.CENTER)
        {
            if (Col != null && Base == PivotBase.COLLIDER)
            {
                xPos = Col.bounds.center.x;
                yPos = Col.bounds.center.y;
            }
            else
            {
                xPos = Rend.bounds.center.x;

                yPos = Rend.bounds.center.y;
            }
        }
        else if (Pivot == PivotPositions.BOTTOM_LEFT)
        {
            if (Col != null && Base == PivotBase.COLLIDER)
            {
                xPos = Col.bounds.min.x;
                yPos = Col.bounds.min.y;
            }
            else
            {
                xPos = Rend.bounds.min.x;
                yPos = Rend.bounds.min.y;
            }
        }
        else if (Pivot == PivotPositions.CENTER_LEFT)
        {
            if (Col != null && Base == PivotBase.COLLIDER)
            {
                xPos = Col.bounds.min.x;
                yPos = Col.bounds.center.y;
            }
            else
            {
                xPos = Rend.bounds.min.x;
                yPos = Rend.bounds.center.y;
            }
        }
        else if (Pivot == PivotPositions.TOP_RIGHT)
        {
            if (Col != null && Base == PivotBase.COLLIDER)
            {
                xPos = Col.bounds.max.x;
                yPos = Col.bounds.max.y;
            }
            else
            {
                xPos = Rend.bounds.max.x;
                yPos = Rend.bounds.max.y;
            }
        }
        else if (Pivot == PivotPositions.BOTTOM_CENTER)
        {
            if (Col != null && Base == PivotBase.COLLIDER)
            {
                xPos = Col.bounds.center.x;
                yPos = Col.bounds.min.y;
            }
            else
            {
                xPos = Rend.bounds.center.x;
                yPos = Rend.bounds.min.y;
            }
        }

        yPos += YOffset;
        xPos += XOffset;

        return new Vector2(xPos, yPos);
    }
}