﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZSortValues : MonoBehaviour
{
    [Range(-1f, 1f)]
    public float XWeight = 0.045f;

    [Range(-1f, 1f)]
    public float YWeight = -0.174f;

    //  Singleton instance
    private static ZSortValues _instance;
    public static ZSortValues Instance
    {
        get
        {
            //  If there is already an instance, return it
            if (_instance != null)
            {
                return _instance;
            }
            //  Otherwise, create an instance of AttribImport on an empty game object if the instance doesn't already exist
            else
            {
                _instance = new GameObject("Z-Sort Vals").AddComponent<ZSortValues>();
                return _instance;
            }
        }
    }
}
