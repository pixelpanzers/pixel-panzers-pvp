﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Transform Target;
    public float Smoothing = 1f;
    public float WorldSizeX = 10;
    public float WorldSizeY = 10;
    private Vector2 Velocity = Vector3.zero;

    private float minX;
    private float maxX;
    private float minY;
    private float maxY;
     
    void Start()
    {
        var vertExtent = Camera.main.orthographicSize;
        var horzExtent = vertExtent * Screen.width / Screen.height;

        // Calculations assume map is position at the origin
        minX = horzExtent - WorldSizeX / 2.0f;
        maxX = WorldSizeX / 2.0f - horzExtent;
        minY = vertExtent - WorldSizeY / 2.0f;
        maxY = WorldSizeY / 2.0f - vertExtent;
    }

    public void LateUpdate()
    {
        if (!Target)
            return;

        Vector3 TPos = Target.position;
        TPos.x = Mathf.Clamp(TPos.x, minX, maxX);
        TPos.y = Mathf.Clamp(TPos.y, minY, maxY);
        TPos.z = -10;

        Vector3 CPos = Vector2.SmoothDamp(transform.position, TPos, ref Velocity, Smoothing);
        CPos.z = -10;

        transform.position = CPos;
    }

    private Vector3 PixelSnap(Vector3 _pos)
    {
        float pX;
        float pY;
        pX = (float)Math.Round(_pos.x, 2);
        pY = (float)Math.Round(_pos.y, 2);

        return new Vector3(pX, pY, _pos.z);
    }
}
