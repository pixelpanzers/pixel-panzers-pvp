﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeZOffset : MonoBehaviour
{
    [Range(-50, 50)]
    public int OffsetChange = 0;

    private void OnTriggerEnter2D(Collider2D _col)
    {
        ZSorter2 Sorter = _col.GetComponent<ZSorter2>();

        if (Sorter != null)
        {
            Sorter.Offset += OffsetChange;
        }
    }

    private void OnTriggerExit2D(Collider2D _col)
    {
        ZSorter2 Sorter = _col.GetComponent<ZSorter2>();

        if (Sorter != null)
        {
            Sorter.Offset -= OffsetChange;
        }
    }
}
