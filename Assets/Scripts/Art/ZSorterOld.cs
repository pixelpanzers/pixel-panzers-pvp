﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ZSorterOld : MonoBehaviour
{
    SpriteRenderer sprite;
    LineRenderer line;
    public int offset = 0;

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
        line = GetComponent<LineRenderer>();
    }

    void Update()
    {
        if (sprite)
            sprite.sortingOrder = (int)((-transform.position.y * 100f) + (offset));
        if (line)
            line.sortingOrder = (int)((-transform.position.y * 100f) + (offset));
    }
}
