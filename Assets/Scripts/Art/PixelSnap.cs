﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class PixelSnap : MonoBehaviour
{
    public bool Static = true;
    private Vector3 StartPos;

    private void Start()
    {
        StartPos = transform.localPosition;
    }
    // Update is called once per frame
    void Update()
    {
        //foreach (GameObject Go in FindObjectsOfType(typeof(GameObject)))
        //{
        //    if (Go.GetComponent<Camera>() != null)
        //        return;

        //    if(Go.GetComponent<Renderer>() != null)
        //        Go.transform.position = Go.transform.position.RoundPos();
        //}

        if(Static) transform.localPosition = StartPos;
        transform.position = transform.position.RoundPos();
    }

    public Vector3 RoundToPixel(Vector3 position)
    {
        float unitsPerPixel = 100;
        if (unitsPerPixel == 0.0f)
            return position;

        Vector2 refScale;
        refScale = new Vector2(UnityEngine.Screen.width / 960, UnityEngine.Screen.height / 540);

        Vector3 result;
        result.x = (Mathf.Round(position.x / unitsPerPixel) * unitsPerPixel);
        result.y = (Mathf.Round(position.y / unitsPerPixel) * unitsPerPixel);
        result.z = (Mathf.Round(position.z / unitsPerPixel) * unitsPerPixel);

        //UnityEngine.Debug.Log(refScale);

        return result;
    }
}
