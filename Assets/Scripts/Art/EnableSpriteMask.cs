﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteMask))]
public class EnableSpriteMask : MonoBehaviour
{
    SpriteMask Mask;
    List<GameObject> CurObjects = new List<GameObject>();

    private void Awake()
    {
        Mask = GetComponent<SpriteMask>();
        Mask.sprite = transform.parent.GetComponent<SpriteRenderer>().sprite;
    }

    private void Start()
    {
        //Mask.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.tag == "PlayerTank" /*|| _col.tag == "Tank"*/ || _col.tag == "OtherOccluded")
        {
            CurObjects.Add(_col.gameObject);
            Mask.enabled = true;
        }
    }

    private void OnTriggerExit2D(Collider2D _col)
    {
        if (_col.tag == "PlayerTank" /*|| _col.tag == "Tank"*/ || _col.tag == "OtherOccluded")
        {
            CurObjects.Remove(_col.gameObject);

            if(CurObjects.Count < 1)
                Mask.enabled = false;
        }
    }

    private void OnTriggerStay2D(Collider2D _col)
    {
        if (_col.tag == "PlayerTank" /*|| _col.tag == "Tank"*/ || _col.tag == "OtherOccluded")
        {
            Mask.enabled = true;
        }
    }
}
