﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI = UnityEngine.UI;
using Utils;
using UnityEngine.U2D;

public class TankHealthUI : MonoBehaviour
{
    public Vector2 Padding;
    public Transform Target;
    public bool OnScreenSprite = false;

    RectTransform RT;

    Tank T;

    private Transform parent;
    private void Start()
    {
        RT = GetComponent<RectTransform>();
        parent = transform.parent;
        T = GetComponentInParent<Tank>();

        //  This prevents this particular UI piece from jittering. Don't ask me why.
        gameObject.SetActive(false);
        gameObject.SetActive(true);

        UIEvents.UIUpdate += UIUpdate;
    }

    void UIUpdate(Tank _curTank)
    {
        if (OnScreenSprite)
        {
            UI.Image Img = transform.Find("Tank HP Foreground").GetComponent<UI.Image>();
            Img.fillAmount = T.PrimaryHp / T.Attr.Primary_hp;
        }
        else
        {
            UI.Text Txt = GetComponentInChildren<UI.Text>();
            UI.Image Img = transform.Find("Tank HP Foreground").GetComponent<UI.Image>();

            Txt.text = T.gameObject.name + " HP: " + T.PrimaryHp;
            Img.fillAmount = T.PrimaryHp / T.Attr.Primary_hp;
        }
    }

    void LateUpdate()
    {
        //  Don't draw the tank HP UI for the current player tank
        if(T == T.GetActivePlayerTank())
        {
            foreach (UI.MaskableGraphic Img in GetComponentsInChildren<UI.MaskableGraphic>())
            {
                Img.enabled = false;
            }

            return;
        }

        //  Get the current camera bounds
        Bounds CamBounds = Camera.main.OrthographicBounds();

        Vector3 CurTar;

        //  Get the bounds of the UI by encapsulating the child elements
        Bounds UIBounds = new Bounds();
        foreach (RectTransform Rect in transform.GetComponentsInChildren<RectTransform>())
        {
            UIBounds.Encapsulate(Rect.rect.min);
            UIBounds.Encapsulate(Rect.rect.max);
        }

        //  Clamp the position of the UI element to the screen bounds
        CurTar = Target.position.Clamp2D(
            CamBounds.max.x - Padding.x - UIBounds.extents.x / 100,
            CamBounds.min.x + Padding.x + UIBounds.extents.x / 100,
            CamBounds.max.y - Padding.y - UIBounds.extents.y / 100,
            CamBounds.min.y + Padding.y + UIBounds.extents.y / 100).RoundPos();

        //  If the clamping didn't change the CurTar position at all, the tank is on screen
        if (Vector2.SqrMagnitude(CurTar - Target.position) < 0.001f)
        {
            foreach (UI.MaskableGraphic Img in GetComponentsInChildren<UI.MaskableGraphic>())
            {
                Img.enabled = OnScreenSprite;
            }
        }
        else
        {
            foreach (UI.MaskableGraphic Img in GetComponentsInChildren<UI.MaskableGraphic>())
            {
                Img.enabled = !OnScreenSprite;
            }
        }

        //  Set the position to the clampted target position
        RT.position = CurTar;
    }
}
