﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI = UnityEngine.UI;

public class CursorReload : MonoBehaviour
{
    UI.Image ReloadLine;
    public Vector2 Offset;

    public Tank CurTank;

    void Start()
    {
        ReloadLine = GetComponent<UI.Image>();
        UIEvents.UIUpdate += UIEvents_UIUpdate;
    }

    private void UIEvents_UIUpdate(Tank _curTank)
    {
        CurTank = _curTank;
    }

    void LateUpdate()
    {
        //  Follow the mouse cursor
        ReloadLine.transform.position = (Vector2)Input.mousePosition + Offset;

        //  Set the reload line fill-ammount to display the reload delay, reload time is in MS
        if (CurTank != null)
            ReloadLine.fillAmount = (Time.timeSinceLevelLoad - CurTank.LastShotTime) / (CurTank.Attr.G1_reload_time / 1000);
        else
            ReloadLine.fillAmount = 1;
    }
}
