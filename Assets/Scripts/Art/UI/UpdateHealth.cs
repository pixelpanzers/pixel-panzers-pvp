﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UI = UnityEngine.UI;

public class UpdateHealth : MonoBehaviour
{
    private UI.Text HPText;
    private UI.Image HPFG;
    private UI.Image HPIcon;
    private float HealthBarLength = 151;

    public void Start()
    {
        UIEvents.UIUpdate += UIUpdate;

        //  Get the HP bar components which need updating
        HPText = transform.Find("Health Number").GetComponent<UI.Text>();
        HPFG = transform.Find("Health Bar FG").GetComponent<UI.Image>();
        HPIcon = transform.Find("Health Bar Icon").GetComponent<UI.Image>();
    }

    public void UIUpdate(Tank _curTank)
    {
        float hp = _curTank.PrimaryHp;
        float maxHp = _curTank.Attr.Primary_hp;
        float refScaleX = Screen.width / Camera.main.GetComponent<PixelPerfectCamera>().refResolutionX;
        float perc = hp / maxHp * HealthBarLength;

        //  Set the current HP text
        HPText.text = hp.ToString();
        
        //  Interpolate the foreground portion of the health bar
        HPFG.GetComponent<RectTransform>().sizeDelta = new Vector2(perc, 4);

        //  Move the icon based on the health bar percentage
        HPIcon.rectTransform.position = new Vector2(HPFG.rectTransform.position.x + (perc * refScaleX), HPFG.rectTransform.position.y);
    }
}
