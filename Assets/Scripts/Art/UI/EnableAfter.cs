﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableAfter : MonoBehaviour
{
    public float Seconds = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DoEnable());
    }

    public IEnumerator DoEnable()
    {
        yield return new WaitForSeconds(Seconds);
        Enable();
    }

    public void Enable()
    {
        if (GetComponent<SpriteRenderer>() != null)
            GetComponent<SpriteRenderer>().enabled = true;

        if (GetComponent<Animator>() != null)
            GetComponent<Animator>().enabled = true;
    }
}
