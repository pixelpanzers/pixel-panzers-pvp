﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI = UnityEngine.UI;

public class UpdateTankName : MonoBehaviour
{
    private UI.Text TankName;

    public void Start()
    {
        //  Subscribe to the UI update event
        UIEvents.UIUpdate += UIUpdate;

        TankName = this.GetComponent<UI.Text>();
    }

    public void UIUpdate(Tank _curTank)
    {
        TankName.text = _curTank.name;
    }
}
