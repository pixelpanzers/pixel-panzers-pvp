﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.Find("UI").gameObject.SetActive(true);
    }
}
