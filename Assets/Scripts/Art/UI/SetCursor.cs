﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI = UnityEngine.UI;

public class SetCursor : MonoBehaviour
{
    UI.Image UICursor;
    public Vector2 Offset;

    void Start()
    {
        UICursor = GetComponent<UI.Image>();
    }

    void LateUpdate()
    {
        UICursor.transform.position = (Vector2)Input.mousePosition + Offset;
        Cursor.visible = false;
    }
}
