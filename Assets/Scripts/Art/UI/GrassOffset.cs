﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassOffset : MonoBehaviour
{
    void Start()
    {
        int StartFrame = (int)Mathf.Round(Mathf.Sin(transform.position.x * 3)) + 1;

        GetComponent<Animator>().Play("bush_animation", 0, (1f / 3) * StartFrame);
    }
}
