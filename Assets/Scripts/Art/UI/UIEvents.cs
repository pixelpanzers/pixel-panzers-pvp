﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void TriggerUIUpdate(Tank _curTank);

public class UIEvents
{
    public static event TriggerUIUpdate UIUpdate;

    public static void Update(Tank _curTank)
    {
        UIUpdate(_curTank);
    }
}
