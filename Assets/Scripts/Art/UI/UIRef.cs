﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRef : MonoBehaviour
{
    public static UIRef Instance
    {
        get;
        private set;
    }

    public static Dictionary<char, CapUI> CapUISets
    {
        get;
        private set;
    }

    // Start is called before the first frame update
    void Start()
    {
        CapUISets = new Dictionary<char, CapUI>();
        Instance = this;

        //  Get all the CapUI sets
        Transform CapParent = transform.Find("Top Bar");
        char[] Ids = { 'A', 'B', 'C', 'D', 'E' };

        foreach(char Id in Ids)
        {
            CapUI CurUI = new CapUI();

            CurUI.LeftFlash = CapParent.Find("CP_" + Id + "_Enemy_Flash").gameObject;
            CurUI.LeftCapturing = CapParent.Find("CP_" + Id + "_Enemy_Capturing").gameObject;
            CurUI.LeftCaptured = CapParent.Find("CP_" + Id + "_Enemy_Captured").gameObject;

            CurUI.RightFlash = CapParent.Find("CP_" + Id + "_Friendly_Flash").gameObject;
            CurUI.RightCapturing = CapParent.Find("CP_" + Id + "_Friendly_Capturing").gameObject;
            CurUI.RightCaptured = CapParent.Find("CP_" + Id + "_Friendly_Captured").gameObject;

            CapUISets.Add(Id, CurUI);
        }
    }
}

public struct CapUI
{
    public GameObject RightFlash;
    public GameObject RightCapturing;
    public GameObject RightCaptured; 

    public GameObject LeftFlash;
    public GameObject LeftCapturing;
    public GameObject LeftCaptured;
}