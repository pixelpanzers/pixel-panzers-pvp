﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

[RequireComponent(typeof(TankAnimations))]
public class Tank : MonoBehaviour
{
    public TankDir Direction = TankDir.RIGHT_FACING;

    public static Tank ActivePlayerTank;
    public bool ActivePlayer
    {
        get => PlayerTank;
    }
     
    private Dictionary<string, TankAttrib> Attributes;
    private TankAnimations Anims;
    
    //  Attributes loaded from the balancing file
    internal TankAttrib Attr { get; private set; }
    internal Rigidbody2D Rb { get; private set; }

    //  If velocity is lower than this it will be zeroed
    internal float MinVelocity = 0.1f;

    //  The position at which the bullets spawn
    internal Transform BulletSpawn;

    //  Refrence to the firing arc object
    internal FiringArc Arc;

    //  The values for the firing arc for this particular tank
    public FiringArcSettings FAS;

    //  HP Values
    public float PrimaryHp = 0;
    public float AmmoRackHp = 0;
    public float TrackHp = 0;
    public float EngineHp = 0;

    private float DamageYOffset = 0.15f;

    private bool PlayerTank = false;
    private bool DebugTank = false;

    //  ------------------------------------------------------------------
    //  Unity Functions
    //  ------------------------------------------------------------------

    private void Start()
    {
        //  Load the tanks attributes
        Attributes = TankAttribManager.Instance.Attribs;
        Attr = Attributes[gameObject.name];
        Anims = GetComponent<TankAnimations>();

        //  Set Rigidbody settings
        Rb = GetComponent<Rigidbody2D>();
        Rb.mass = Attr.Momentum_mass;
        Rb.drag = Attr.Linear_drag / 10;
        //Rb.drag = 0;

        //  Get the bullet spawn point for firing
        BulletSpawn = transform.Find("Bullet");

        //  Set the base HP values
        PrimaryHp = Attr.Primary_hp;
        TrackHp = Attr.Track_hp;
        AmmoRackHp = Attr.Ammo_rack_hp;
        EngineHp = Attr.Engine_hp;

        //  Get the firing arc script in child transforms
        Arc = FiringArc.Instance;
        Arc.SetEnabled(false);

        //  Simulate a late fixed update function
        StartCoroutine(LateFixedUpdate());

        //  Track the existing tanks
        TankList.Instance.ActiveTanks.Add(this);
    }

    internal virtual void Update()
    {
        PerformAnims();

        //  Don't allow any interaction if the tank is tracked
        if (IsTracked) return;

        if (PlayerTank)
        {
            Fire();

            if (Input.GetKeyDown(KeyCode.T))
                Anims.DrawDamage(Convert.ToInt32(PrimaryHp).ToString(), DamageType.PIERCING, new Vector2(transform.position.x, transform.position.y + DamageYOffset));

            if (Input.GetKeyDown(KeyCode.J))
            {
                //SetTracked(true);
                //SetEngineFire(true);
                //SetRackFire(true);
            }
        }

        //  Cap the speed based on the direction the tank is moving
        if (Direction == TankDir.RIGHT_FACING)
            CapSpeedRightFacing();
        else
            CapSpeedLeftFacing();

        ZeroSpeed();
    }

    Vector2 DragScrub;
    internal virtual void FixedUpdate()
    {
        //  Don't do any movement if the player is tracked
        if (IsTracked) return;

        //  The ammount of velocity scrubbed off in the physics update by drag
        DragScrub = Rb.velocity - Rb.velocity * (1 - Time.deltaTime * Rb.drag);

        if (PlayerTank)
        {
            Move();
        }
    }

    internal IEnumerator LateFixedUpdate()
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();

            //if (IsAcceleratingX())
            //    Rb.velocity += new Vector2(DragScrub.x, 0);

            //if (IsAcceleratingY())
            //    Rb.velocity += new Vector2(0, DragScrub.y);
        }
    }

    //  Debug change player tank
    internal virtual void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(2) && gameObject.tag == "PlayerTank")
        {
            SetAllDisabled();
            EnableTank();
        }
    }

    //  For displaying debug info
    void OnGUI()
    {
        if (Input.GetKeyDown(KeyCode.T))
            DebugTank = !DebugTank;

        if (!PlayerTank || !DebugTank)
            return;

        GUI.Label(new Rect(10, 10, 500, 500), "Current Tank: " + gameObject.name);
        GUI.Label(new Rect(10, 30, 500, 500), "X-Velocity: " + (Rb.velocity.x * 100).ToString());
        GUI.Label(new Rect(10, 50, 500, 500), "Y-Velocity: " + (Rb.velocity.y * 100).ToString());
    }

    //  ------------------------------------------------------------------
    //  Class Functions
    //  ------------------------------------------------------------------

    public void EnableTank()
    {
        PlayerTank = true;

        //  Set the new camera target
        Camera.main.GetComponent<FollowCamera>().Target = transform.Find("Camera Target");

        if(Attr.Firing_arc_enabled == 1)
            Arc.SetEnabled(true);

        //  Set the static attribute reference for the current tank values
        ActivePlayerTank = this;

        //  Send the UI update event
        UIUpdate();
    }

    internal virtual void UIUpdate()
    {
        UIEvents.Update(ActivePlayerTank);
    }

    public float LastShotTime = -1f;
    internal virtual void Fire()
    {
        if(Input.GetMouseButtonDown(0) && ValidShot())
        {
            //  Don't shoot if the tank is reloading
            if (Time.timeSinceLevelLoad < LastShotTime + (Attr.G1_reload_time / 1000))
                return;

            //  Get the direction, angle and force for the bullet
            Vector2 MousePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 SPoint = new Vector3(BulletSpawn.position.x, BulletSpawn.position.y);
            Vector2 EPoint = new Vector3(MousePoint.x, MousePoint.y);
            Vector2 Force = (EPoint - SPoint).normalized * Attr.G1_shot_speed_velocity / 100f;
            
            //  How far to continue the current trajectory of the projectile past the mouse cursor, in pixels
            int MouseOffset = 50;

            //  Angle from the bullet spawn point to the mouse cursor location
            float LineAngle = Mathf.Rad2Deg * Mathf.Atan2(EPoint.y - SPoint.y, EPoint.x - SPoint.x);
            Line ShotLine = MathK.LineFromAngle(BulletSpawn.position, LineAngle, (EPoint - SPoint).magnitude + (MouseOffset / 100f));

            //  Play the relevent animations
            Anims.PlayFiringDust(Direction);
            Anims.PlayGun1Anim();
            Anims.PlayFiringSmoke();

            //  Instantiate the bullet and set it's position/rotation
            GameObject Bullet = Instantiate(Resources.Load<GameObject>("medium_bullet"));
            Bullet.transform.position = BulletSpawn.position;
            Bullet.transform.rotation = Quaternion.Euler(0, 0, LineAngle);

            //  Add the force to the projectile
            Bullet.GetComponent<Rigidbody2D>().AddForce(Force, ForceMode2D.Impulse);

            //  The line angle, but adjusted for left facing tanks when necessary
            float GlancingAngle;
            if(Direction == TankDir.LEFT_FACING)
                GlancingAngle = Mathf.Rad2Deg * Mathf.Atan2(SPoint.y - EPoint.y, SPoint.x - EPoint.x);
            else
                GlancingAngle = Mathf.Rad2Deg * Mathf.Atan2(EPoint.y - SPoint.y, EPoint.x - SPoint.x);

            //  Check the firing angles to see if this is a glancing blow
            bool Glancing;
            if (GlancingAngle < Attr.Glancing_angle && GlancingAngle > -Attr.Glancing_angle)
                Glancing = true;
            else
                Glancing = false;

            Debug.Log(Glancing);
            Debug.Log(GlancingAngle);

            //  Give the projectile it's target point incase of a miss
            Projectile BulletP = Bullet.GetComponent<Projectile>();

            BulletP.SetTarget(ShotLine.EndPoint);
            BulletP.Instantiate(this, Glancing);

            //  Set the last fired time for tracking shot delay
            LastShotTime = Time.timeSinceLevelLoad;
        }
    }

    internal virtual void PerformAnims()
    {
        //  Trigger dust animations based on RB velocity
        if (Rb.velocity.x < 0)
        {
            Anims.SetDustBackward(2f * MaxSpeedPercX());
            Anims.SetDustForward(0);
        }
        else if (Rb.velocity.x > 0 || Mathf.Abs(Rb.velocity.y) > 0)
        {
            Anims.SetDustForward(2f * MaxSpeedPerc());
            Anims.SetDustBackward(0);
        }
        else
        {
            Anims.SetDustBackward(0);
            Anims.SetDustForward(0);
        }

        //  Set the animation speed of the track/dust animations based on the tanks movement speed
        Anims.SetDrivingAnim(1f * MaxSpeedPerc());
    }

    public void SetAllDisabled()
    {
        foreach (Tank t in TankList.Instance.ActiveTanks)
        {
            t.PlayerTank = false;
            t.Arc.SetEnabled(false);
        }
    }

    public void Move()
    {
        //  Add force based on the players input
        float YForce = Input.GetAxis("Vert") * Attr.Accel_y;
        float XForce = 0f;

        //  Force to exactly counter drag
        Vector2 AntiDrag = DragScrub / Time.deltaTime * Rb.mass;
        float AntiDragPerc = 1f + (DragScrub.x / Rb.velocity.x);

        //  Distinguish between left/right facing to determine reverse acceleration
        if (Direction == TankDir.RIGHT_FACING)
            MoveRightFacing(ref XForce);
        else
            MoveLeftFacing(ref XForce);

        //  Counter the force of drag while accelerating
        if (XForce == 0)
            AntiDrag.x = 0;

        if (YForce == 0)
            AntiDrag.y = 0;
        
        //  Apply the forces to the object
        Vector2 Force = new Vector2(XForce, YForce);

        Rb.AddForce((Force * 60) + AntiDrag, ForceMode2D.Force);

        //Change drag when there is no input
        if (Force == Vector2.zero)
            Rb.drag = Attr.Linear_drag * Attr.Stop_speed_multip / 10;
        else
            Rb.drag = Attr.Linear_drag / 10;

        //  Debug
        if (Input.GetKey(KeyCode.Space))
            Rb.velocity = Vector2.zero;
    }

    void MoveLeftFacing(ref float _xForce)
    {
        //  Moving forward
        if (Rb.velocity.x < 0)
        {
            _xForce = Input.GetAxis("Horz") * Attr.Accel_x;
        }
        //  Moving backwards
        else if (Rb.velocity.x > 0)
        {
            _xForce = Input.GetAxis("Horz") * Attr.Accel_x * Attr.Reverse_speed_multip;
        }
        else
        {
            _xForce = Input.GetAxis("Horz") * Attr.Accel_x;
        }
    }

    void MoveRightFacing(ref float _xForce)
    {
        //  Moving forward
        if (Rb.velocity.x > 0)
        {
            _xForce = Input.GetAxis("Horz") * Attr.Accel_x;
        }
        //  Moving backwards
        else if (Rb.velocity.x < 0)
        {
            _xForce = Input.GetAxis("Horz") * Attr.Accel_x * Attr.Reverse_speed_multip;
        }
        else
        {
            _xForce = Input.GetAxis("Horz") * Attr.Accel_x;
        }
    }

    private void ZeroSpeed()
    {
        //  Zero velocity if it's below the threshold
        if (Input.GetAxis("Horz") == 0)
        {
            if (Mathf.Abs(Rb.velocity.x) < MinVelocity)
            {
                Rb.velocity = new Vector2(0, Rb.velocity.y);
            }
        }


        if (Input.GetAxis("Vert") == 0)
        {
            if (Mathf.Abs(Rb.velocity.y) < MinVelocity)
            {
                Rb.velocity = new Vector2(Rb.velocity.x, 0);
            }
        }
    }

    private void CapSpeedLeftFacing()
    {
        //  Cap forward velocity
        if (Rb.velocity.x * 100 < -Attr.Forward_max)
        {
            Rb.velocity = new Vector2(-Attr.Forward_max / 100 * Time.timeScale, Rb.velocity.y);
        }

        //  Cap reverse velocity
        if (Rb.velocity.x * 100 > Attr.Reverse_max)
        {
            Rb.velocity = new Vector2(Attr.Reverse_max / 100 * Time.timeScale, Rb.velocity.y);
        }

        //  Cap upward velocity
        if (Rb.velocity.y * 100 > Attr.Y_axis_max)
        {
            Rb.velocity = new Vector2(Rb.velocity.x, Attr.Y_axis_max / 100 * Time.timeScale);
        }

        //  Cap downward velocity
        if (Rb.velocity.y * 100 < -Attr.Y_axis_max)
        {
            Rb.velocity = new Vector2(Rb.velocity.x, -Attr.Y_axis_max / 100 * Time.timeScale);
        }
    }

    //  Speeds velocity is multiplied by 100 to make them easier to tune, velocity is typically well under 0
    private void CapSpeedRightFacing()
    {
        //  Cap forward velocity
        if (Rb.velocity.x * 100 > Attr.Forward_max)
        {
            Rb.velocity = new Vector2(Attr.Forward_max / 100 * Time.timeScale, Rb.velocity.y);
        }

        //  Cap reverse velocity
        if (Rb.velocity.x * 100 < -Attr.Reverse_max)
        {
            Rb.velocity = new Vector2(-Attr.Reverse_max / 100 * Time.timeScale, Rb.velocity.y);
        }

        //  Cap upward velocity
        if (Rb.velocity.y * 100 > Attr.Y_axis_max)
        {
            Rb.velocity = new Vector2(Rb.velocity.x, Attr.Y_axis_max / 100 * Time.timeScale);
        }

        //  Cap downward velocity
        if (Rb.velocity.y * 100 < -Attr.Y_axis_max)
        {
            Rb.velocity = new Vector2(Rb.velocity.x, -Attr.Y_axis_max / 100 * Time.timeScale);
        }
    }

    public bool InputInMoveDirectionX()
    {
        if (Input.GetAxis("Horz") > 0 && Rb.velocity.x < 0)
            return true;
        else if (Input.GetAxis("Horz") > 0 && Rb.velocity.x > 0)
            return true;

        return false;
    }

    public bool InputInMoveDirectionY()
    {
        if (Input.GetAxis("Vert") > 0 && Rb.velocity.y < 0)
            return true;
        else if (Input.GetAxis("Vert") > 0 && Rb.velocity.y > 0)
            return true;

        return false;
    }

    public void Die()
    {
        this.StopAllCoroutines();
    }

    public float ChangeHP(HealthType _type, float _change, bool _displayAnim = false, DamageType _dt = DamageType.PIERCING)
    {
        float NewHp;

        if (_type == HealthType.PRIMARY)
        {
            PrimaryHp = Mathf.Clamp(PrimaryHp + _change, 0, Attr.Primary_hp);
            NewHp = PrimaryHp;
        }
        else if (_type == HealthType.ENGINE)
        {
            EngineHp = Mathf.Clamp(EngineHp + _change, 0, Attr.Engine_hp);
            NewHp = EngineHp;
        }
        else if (_type == HealthType.AMMO_RACK)
        {
            AmmoRackHp = Mathf.Clamp(AmmoRackHp + _change, 0, Attr.Ammo_rack_hp);
            NewHp = AmmoRackHp;
        }
        else
        {
            TrackHp = Mathf.Clamp(TrackHp + _change, 0, Attr.Track_hp);
            NewHp = TrackHp;
        }

        UIUpdate();


        if (_displayAnim)
            Anims.DrawDamage(Convert.ToInt32(_change).ToString(), _dt, new Vector2(transform.position.x, transform.position.y + DamageYOffset));

        return NewHp;
    }

    public float Damage(HealthType _type, float _change, string _gunCalibre, string _tankClass, bool _displayAnim = false)
    {
        float NewHp;
        float AdjustedChange = _change;
        DamageType DT = GetDamageType(_gunCalibre, _tankClass);

        if (_type == HealthType.PRIMARY)
        {
            if (DT == DamageType.NONE) return PrimaryHp;
            if (DT == DamageType.SPALLING) AdjustedChange *= 0.5f;

            NewHp = PrimaryHp -= AdjustedChange;
        }
        else if (_type == HealthType.ENGINE)
        {
            if (DT == DamageType.NONE) return EngineHp;
            if (DT == DamageType.SPALLING) AdjustedChange *= 0.5f;

            NewHp = EngineHp -= AdjustedChange;

            if (NewHp <= 0) SetEngineFire(true);
        }
        else if (_type == HealthType.AMMO_RACK)
        {
            if (DT == DamageType.NONE) return AmmoRackHp;
            if (DT == DamageType.SPALLING) AdjustedChange *= 0.5f;

            NewHp = AmmoRackHp -= AdjustedChange;

            if (NewHp <= 0) SetRackFire(true);
        }
        else
        {
            if (DT == DamageType.NONE) return TrackHp;
            if (DT == DamageType.SPALLING) AdjustedChange *= 0.5f;

            NewHp = TrackHp -= AdjustedChange;

            if (NewHp <= 0) SetTracked(true);
        }

        //  Display the damage animation
        if (_displayAnim)
            Anims.DrawDamage(Convert.ToInt32(_change).ToString(), DT, new Vector2(transform.position.x, transform.position.y + DamageYOffset));

        //  Send the UI update - this damage could have changed the information which needs to be displayed
        UIUpdate();

        return NewHp;
    }

    internal float TrackedStartTime = -1;
    internal bool IsTracked = false;
    private bool DisableTrackingStarted = false;

    public IEnumerator DisableTracking()
    {
        yield return new WaitForSeconds(Attr.Tracked_time);

        TrackHp = Attr.Track_hp;
        SetTracked(false);
        DisableTrackingStarted = false;
    }

    public void SetTracked(bool _state)
    {
        //  Return if the tank is being tracked once the timer has passed
        if (_state)
        {
            if(DisableTrackingStarted)
                StopCoroutine("DisableTracking");

            DisableTrackingStarted = true;
            StartCoroutine("DisableTracking");
        }
        IsTracked = _state;

        //  Enable the tracked art changes
        transform.Find("driving").gameObject.GetComponent<SpriteRenderer>().enabled =!_state;
        transform.Find("tracked").gameObject.SetActive(_state);
    }

    public IEnumerator EngineFireDamage()
    {
        yield return new WaitForSeconds(Attr.Engine_fire_freq);
        Damage(HealthType.PRIMARY, Attr.Engine_fire_damage, Attr.G1_gun_calibre, Attr.Weight_class, true);

        if(PrimaryHp > 0) StartCoroutine("EngineFireDamage");
    }

    public void SetEngineFire(bool _state)
    {
        if (_state) StartCoroutine("EngineFireDamage");
        else StopCoroutine("EngineFireDamage");

        transform.Find("engine_fire").gameObject.SetActive(_state);
    }

    public void SetRackFire(bool _state)
    {
        transform.Find("ammo_cookoff").gameObject.SetActive(_state);
        Damage(HealthType.PRIMARY, PrimaryHp, Attr.G1_gun_calibre, Attr.Weight_class, true);
    }

    public float MaxSpeedPerc()
    {
        return (MaxSpeedPercX() + MaxSpeedPercY()) / 2;
    }

    public float MaxSpeedPercX()
    {
        return (Mathf.Abs(Rb.velocity.x) * 100) / (Attr.Forward_max);
    }

    public float MaxSpeedPercY()
    {
        return (Mathf.Abs(Rb.velocity.y) * 100) / (Attr.Y_axis_max);
    }

    public bool IsMoving()
    {
        return Rb.velocity == Vector2.zero;
    }

    public TankAttrib GetAttr()
    {
        return Attributes[gameObject.name];
    }

    public TankAttrib GetAttr(string _name)
    {
        return Attributes[_name];
    }

    public Tank GetActivePlayerTank()
    {
        return ActivePlayerTank;
    }

    public bool ValidShot()
    {
        Vector2 MouseWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (MouseWorld.IsInPolygon(Arc.FirePoly) || Attr.Firing_arc_enabled == 0f)
            return true;

        return false;
    }

    public static DamageType GetDamageType(string _gunCalibre, string _tankClass)
    {
        //  Light Guns
        if (_gunCalibre == "MachineGuns/Autocannons" || _gunCalibre == "37mm" || _gunCalibre == "40mm 2pdr" || _gunCalibre == "47mm")
        {
            if (_tankClass == "LT")
            {
                return DamageType.PIERCING;
            }
            else if (_tankClass == "MT")
            {
                return DamageType.SPALLING;
            }
            else if (_tankClass == "HT")
            {
                return DamageType.NONE;
            }
            else if (_tankClass == "TD")
            {
                return DamageType.NONE;
            }
            else
            {
                Debug.LogError("An unknown tank class has been used");
                return DamageType.NONE;
            }
        }
        //  Medium Guns
        else if (_gunCalibre == "50mm" || _gunCalibre == "57mm (6pdr)" || _gunCalibre == "75mm Sherman" || _gunCalibre == "75mm PzIV" || _gunCalibre == "75mm Long L/70")
        {
            if (_tankClass == "LT")
            {
                return DamageType.PIERCING;
            }
            else if (_tankClass == "MT")
            {
                return DamageType.PIERCING;
            }
            else if (_tankClass == "HT")
            {
                return DamageType.SPALLING;
            }
            else if (_tankClass == "TD")
            {
                return DamageType.NONE;
            }
            else
            {
                Debug.LogError("An unknown tank class has been used");
                return DamageType.NONE;
            }
        }
        //  Heavy Guns
        else if (_gunCalibre == "77mm Comet" || _gunCalibre == "76mm (17pdr)" || _gunCalibre == "84mm (20pdr)" || _gunCalibre == "85mm (Soviet)" ||
            _gunCalibre == "88" || _gunCalibre == "90" || _gunCalibre == "94mm 32PDR" || _gunCalibre == "100mm Soviet")
        {
            if (_tankClass == "LT")
            {
                return DamageType.PIERCING;
            }
            else if (_tankClass == "MT")
            {
                return DamageType.PIERCING;
            }
            else if (_tankClass == "HT")
            {
                return DamageType.PIERCING;
            }
            else if (_tankClass == "TD")
            {
                return DamageType.SPALLING;
            }
            else
            {
                Debug.LogError("An unknown tank class has been used");
                return DamageType.NONE;
            }
        }
        //  Assault Guns
        else if (_gunCalibre == "105" || _gunCalibre == "122 Soviet" || _gunCalibre == "128" || _gunCalibre == "150" || _gunCalibre == "152" || _gunCalibre == "380")
        {
            if (_tankClass == "LT")
            {
                return DamageType.PIERCING;
            }
            else if (_tankClass == "MT")
            {
                return DamageType.PIERCING;
            }
            else if (_tankClass == "HT")
            {
                return DamageType.PIERCING;
            }
            else if (_tankClass == "TD")
            {
                return DamageType.PIERCING;
            }
            else
            {
                Debug.LogError("An unknown tank class has been used");
                return DamageType.NONE;
            }
        }
        else
        {
            Debug.LogError("An unknown gun calibre has been used");
            return DamageType.NONE;
        }
    }

    internal bool IsAccelerating()
    {
        return Input.GetAxis("Vert") + Input.GetAxis("Horz") != 0;
    }

    internal bool IsAcceleratingY()
    {
        return Input.GetAxis("Vert") != 0;
    }

    internal bool IsAcceleratingX()
    {
        return Input.GetAxis("Horz") != 0;
    }

    public enum HealthType
    {
        PRIMARY,
        TRACK,
        ENGINE,
        AMMO_RACK
    }

    public enum TankDir
    {
        LEFT_FACING,
        RIGHT_FACING
    }

    public enum DamageType
    {
        SPALLING,
        PIERCING,
        NONE
    }
}
