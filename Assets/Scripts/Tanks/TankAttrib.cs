﻿using System;
using System.Reflection;
using UnityEngine;

[System.Serializable]
public class TankAttrib
{
    private float
        forward_max,
        reverse_max,
        y_axis_max,
        reverse_speed_multip,
        accel_x,
        accel_y,
        momentum_mass,
        linear_drag,
        stop_speed_multip,
        primary_hp,
        engine_hp,
        ammo_rack_hp,
        track_hp,
        ar_front,
        ar_side,
        ar_back,
        g1_ap,
        g1_he,
        g1_reload_time,
        g1_firing_arc,
        g1_shot_speed_velocity,
        g2_ap,
        g2_he,
        g2_reload_time,
        g2_firing_arc,
        g2_shot_speed_velocity,
        mass,
        glancing_angle,
        rebuild_time,
        bp_salvage_reward,
        bp_build_cost,
        bp_destroy_reward,
        tracked_time,
        engine_fire_freq,
        engine_fire_damage,
        firing_arc_enabled;

    private string
        russian,
        indonesian,
        spanish,
        french,
        german,
        turkish,
        g1_gun_calibre,
        g2_gun_calibre,
        weight_class;

    #region PROPERTIES

    public float Forward_max
    {
        get
        {
            return forward_max;
        }
    }

    public float Reverse_max
    {
        get
        {
            return reverse_max;
        }
    }

    public float Y_axis_max
    {
        get
        {
            return y_axis_max;
        }
    }

    public float Momentum_mass
    {
        get
        {
            return momentum_mass;
        }
    }

    public float Primary_hp
    {
        get
        {
            return primary_hp;
        }
    }

    public float Engine_hp
    {
        get
        {
            return engine_hp;
        }
    }

    public float Ammo_rack_hp
    {
        get
        {
            return ammo_rack_hp;
        }
    }

    public float Track_hp
    {
        get
        {
            return track_hp;
        }
    }

    public float Ar_front
    {
        get
        {
            return ar_front;
        }
    }

    public float Ar_side
    {
        get
        {
            return ar_side;
        }
    }

    public float Ar_back
    {
        get
        {
            return ar_back;
        }
    }

    public float G1_ap
    {
        get
        {
            return g1_ap;
        }
    }

    public float G1_he
    {
        get
        {
            return g1_he;
        }
    }

    public float G1_reload_time
    {
        get
        {
            return g1_reload_time;
        }
    }

    public string G1_gun_calibre
    {
        get
        {
            return g1_gun_calibre;
        }
    }

    public float G1_firing_arc
    {
        get
        {
            return g1_firing_arc;
        }
    }

    public float G1_shot_speed_velocity
    {
        get
        {
            return g1_shot_speed_velocity;
        }
    }

    public float G2_ap
    {
        get
        {
            return g2_ap;
        }
    }

    public float G2_he
    {
        get
        {
            return g2_he;
        }
    }

    public float G2_reload_time
    {
        get
        {
            return g2_reload_time;
        }
    }

    public string G2_gun_calibre
    {
        get
        {
            return g2_gun_calibre;
        }
    }

    public float G2_firing_arc
    {
        get
        {
            return g2_firing_arc;
        }
    }

    public float G2_shot_speed_velocity
    {
        get
        {
            return g2_shot_speed_velocity;
        }
    }

    public float Mass
    {
        get
        {
            return mass;
        }
    }

    public float Rebuild_time
    {
        get
        {
            return rebuild_time;
        }
    }

    public float Bp_salvage_reward
    {
        get
        {
            return bp_salvage_reward;
        }
    }

    public float Bp_build_cost
    {
        get
        {
            return bp_build_cost;
        }
    }

    public float Bp_destroy_reward
    {
        get
        {
            return bp_destroy_reward;
        }
    }

    public string Russian
    {
        get
        {
            return russian;
        }
    }

    public string Indonesian
    {
        get
        {
            return indonesian;
        }
    }

    public string Spanish
    {
        get
        {
            return spanish;
        }
    }

    public string French
    {
        get
        {
            return french;
        }
    }

    public string German
    {
        get
        {
            return german;
        }
    }

    public string Turkish
    {
        get
        {
            return turkish;
        }
    }

    public float Accel_x
    {
        get
        {
            return accel_x;
        }

        set
        {
            accel_x = value;
        }
    }

    public float Accel_y
    {
        get
        {
            return accel_y;
        }

        set
        {
            accel_y = value;
        }
    }

    public float Linear_drag
    {
        get
        {
            return linear_drag;
        }

        set
        {
            linear_drag = value;
        }
    }

    public float Reverse_speed_multip
    {
        get
        {
            return reverse_speed_multip;
        }

        set
        {
            reverse_speed_multip = value;
        }
    }

    public float Stop_speed_multip
    {
        get => stop_speed_multip;
        set => stop_speed_multip = value;
    }

    public string Weight_class
    {
        get => weight_class;
        set => weight_class = value;
    }

    public float Tracked_time
    {
        get => tracked_time;
        set => tracked_time = value;
    }

    public float Engine_fire_damage
    {
        get => engine_fire_damage;
        set => engine_fire_damage = value;
    }

    public float Engine_fire_freq
    {
        get => engine_fire_freq;
        set => engine_fire_freq = value;
    }

    public float Glancing_angle
    {
        get => glancing_angle;
        set => glancing_angle = value;
    }
    public float Firing_arc_enabled
    {
        get => firing_arc_enabled;
        set => firing_arc_enabled = value;
    }

    #endregion

    public static void SetAttrib(TankAttrib _inst, int _index, object _val)
    {
        //  The spreadsheet is in the same order as the AttribType enum values
        //  The TankAttrib class values are all the same as the AttribType enum values
        //  The var names are in lowercase, so they need to be accessed as such
        string varName = ((AttribType)_index).ToString().ToLower();
        //Debug.Print(varName);
        //  Define the type being reflected
        Type type = typeof(TankAttrib);
        //  Get the field using the varName
        FieldInfo field = type.GetField(varName, BindingFlags.NonPublic | BindingFlags.Instance);

        //  If the field exists (helps omit unnecessary headers)
        if (field != null)
        {
            //  Cast the _val object to the correct type and set the field to the correct value
            if (field.FieldType.FullName == "System.Single")
            {
                //Debug.Log(varName);
                field.SetValue(_inst, float.Parse(_val.ToString()));
            }
            else if (field.FieldType.FullName == "System.String")
            {
                //Debug.Log(varName);
                field.SetValue(_inst, _val.ToString());
            }
        }
    }
}

//  This must match the Stats column in the balancing sheet
public enum AttribType
{
    MOVEMENT, // Must always be first
    FORWARD_MAX,
    REVERSE_MAX,
    Y_AXIS_MAX,
    REVERSE_SPEED_MULTIP,
    ACCEL_X,
    ACCEL_Y,
    MOMENTUM_MASS,
    LINEAR_DRAG,
    STOP_SPEED_MULTIP,
    HIT_POINTS,
    PRIMARY_HP,
    ENGINE_HP,
    AMMO_RACK_HP,
    TRACK_HP,
    ARMOUR,
    AR_FRONT,
    AR_SIDE,
    AR_BACK,
    GUNS,
    GUN_1,
    G1_AP,
    G1_HE,
    G1_RELOAD_TIME,
    G1_GUN_CALIBRE,
    G1_FIRING_ARC,
    G1_SHOT_SPEED_VELOCITY,
    GUN_2,
    G2_AP,
    G2_HE,
    G2_RELOAD_TIME,
    G2_GUN_CALIBRE,
    G2_FIRING_ARC,
    G2_SHOT_SPEED_VELOCITY,
    GENERAL,
    MASS,
    GLANCING_ANGLE,
    REBUILD_TIME,
    BP_SALVAGE_REWARD,
    BP_BUILD_COST,
    BP_DESTROY_REWARD,
    TRACKED_TIME,
    ENGINE_FIRE_FREQ,
    ENGINE_FIRE_DAMAGE,
    FIRING_ARC_ENABLED,
    LOCALIZATION,
    RUSSIAN,
    INDONESIAN,
    SPANISH,
    FRENCH,
    GERMAN,
    TURKISH,
    WEIGHT_CLASS // Must always be last
}