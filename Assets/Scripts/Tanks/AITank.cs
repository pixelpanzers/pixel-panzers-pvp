﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using static Tank;

public class AITank : MonoBehaviour
{
    public TankDir Direction = TankDir.RIGHT_FACING;
    private Dictionary<string, TankAttrib> Attributes;
    
    //  Attributes loaded from the balancing file
    internal TankAttrib Attr { get; private set; }

    internal float PrimaryHp;
    internal float EngineHp;
    internal float TrackHp;
    internal float AmmoRackHp;

    internal Transform BulletSpawn;

    internal int DamageYOffset;

    internal TankAnimations Anims;

    internal virtual void Start()
    {
        //  Load the tanks attributes
        Attributes = TankAttribManager.Instance.Attribs;
        Attr = Attributes[gameObject.name];

        Anims = GetComponent<TankAnimations>();

        //  Get the bullet spawn point for firing
        BulletSpawn = transform.Find("Bullet");
    }

    internal virtual void Update()
    {

    }
    
    internal virtual void Shoot(Vector2 _targetPoint)
    {
        //  Get the direction, angle and force for the bullet
        Vector2 SPoint = new Vector3(BulletSpawn.position.x, BulletSpawn.position.y);
        Vector2 EPoint = new Vector3(_targetPoint.x, _targetPoint.y);
        Vector2 Force = (EPoint - SPoint).normalized * Attr.G1_shot_speed_velocity / 100f;

        //  How far to continue the current trajectory of the projectile past the mouse cursor, in pixels
        int MouseOffset = 50;

        //  Angle from the bullet spawn point to the mouse cursor location
        float LineAngle = Mathf.Rad2Deg * Mathf.Atan2(EPoint.y - SPoint.y, EPoint.x - SPoint.x);
        Line ShotLine = MathK.LineFromAngle(BulletSpawn.position, LineAngle, (EPoint - SPoint).magnitude + (MouseOffset / 100f));

        //  Play the relevent animations
        Anims.PlayGun1Anim();
        Anims.PlayFiringSmoke();

        //  Instantiate the bullet and set it's position/rotation
        GameObject Bullet = Instantiate(Resources.Load<GameObject>("medium_bullet"));
        Bullet.transform.position = BulletSpawn.position;
        Bullet.transform.rotation = Quaternion.Euler(0, 0, LineAngle);

        //  Add the force to the projectile
        Bullet.GetComponent<Rigidbody2D>().AddForce(Force, ForceMode2D.Impulse);

        //  Give the projectile it's target point incase of a miss
        Projectile BulletP = Bullet.GetComponent<Projectile>();

        BulletP.SetTarget(ShotLine.EndPoint);
        BulletP.Instantiate(this, false);
    }

    public float ChangeHP(HealthType _type, float _change, bool _displayAnim = false, DamageType _dt = DamageType.PIERCING)
    {
        float NewHp;

        if (_type == HealthType.PRIMARY)
        {
            PrimaryHp = Mathf.Clamp(PrimaryHp + _change, 0, Attr.Primary_hp);
            NewHp = PrimaryHp;
        }
        else if (_type == HealthType.ENGINE)
        {
            EngineHp = Mathf.Clamp(EngineHp + _change, 0, Attr.Engine_hp);
            NewHp = EngineHp;
        }
        else if (_type == HealthType.AMMO_RACK)
        {
            AmmoRackHp = Mathf.Clamp(AmmoRackHp + _change, 0, Attr.Ammo_rack_hp);
            NewHp = AmmoRackHp;
        }
        else
        {
            TrackHp = Mathf.Clamp(TrackHp + _change, 0, Attr.Track_hp);
            NewHp = TrackHp;
        }

        if (_displayAnim)
            Anims.DrawDamage(Convert.ToInt32(_change).ToString(), _dt, new Vector2(transform.position.x, transform.position.y + DamageYOffset));

        return NewHp;
    }

    public float Damage(HealthType _type, float _change, string _gunCalibre, string _tankClass, bool _displayAnim = false)
    {
        float NewHp;
        float AdjustedChange = _change;
        DamageType DT = GetDamageType(_gunCalibre, _tankClass);

        if (_type == HealthType.PRIMARY)
        {
            if (DT == DamageType.NONE) return PrimaryHp;
            if (DT == DamageType.SPALLING) AdjustedChange *= 0.5f;

            NewHp = PrimaryHp -= AdjustedChange;
        }
        else if (_type == HealthType.ENGINE)
        {
            if (DT == DamageType.NONE) return EngineHp;
            if (DT == DamageType.SPALLING) AdjustedChange *= 0.5f;

            NewHp = EngineHp -= AdjustedChange;

            if (NewHp <= 0) SetEngineFire(true);
        }
        else if (_type == HealthType.AMMO_RACK)
        {
            if (DT == DamageType.NONE) return AmmoRackHp;
            if (DT == DamageType.SPALLING) AdjustedChange *= 0.5f;

            NewHp = AmmoRackHp -= AdjustedChange;

            if (NewHp <= 0) SetRackFire(true);
        }
        else
        {
            if (DT == DamageType.NONE) return TrackHp;
            if (DT == DamageType.SPALLING) AdjustedChange *= 0.5f;

            NewHp = TrackHp -= AdjustedChange;

            if (NewHp <= 0) SetTracked(true);
        }

        //  Display the damage animation
        if (_displayAnim)
            Anims.DrawDamage(Convert.ToInt32(_change).ToString(), DT, new Vector2(transform.position.x, transform.position.y + DamageYOffset));

        return NewHp;
    }

    internal virtual void SetEngineFire(bool _state) { }
    internal virtual void SetRackFire(bool _state) { }
    internal virtual void SetTracked(bool _state) { }
}
