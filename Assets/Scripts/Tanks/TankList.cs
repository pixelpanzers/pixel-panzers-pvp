﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankList
{
    public List<Tank> ActiveTanks;

    //  Singleton instance
    private static TankList _instance;
    public static TankList Instance
    {
        get
        {
            //  If there is already an instance, return it
            if (_instance != null)
            {
                return _instance;
            }
            //  Otherwise, create an instance of AttribImport on an empty game object if the instance doesn't already exist
            else
            {
                _instance = new TankList();
                return _instance;
            }
        }
    }

    private TankList()
    {
        ActiveTanks = new List<Tank>();
    }
}
