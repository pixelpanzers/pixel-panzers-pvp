﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float DistForMiss = 0.1f;

    private TankAttrib Firedby;
    private Vector2 TargetPosition = Vector2.one * 99999;
    private bool Glancing;
    private bool HitActivated = false;
    private Collider2D HitCol;

	void Update ()
    {
        if (HitActivated) return;

        if (Vector3.Distance(transform.position, TargetPosition) < DistForMiss)
        {
            Miss(transform.position);
        }
	}

    private void Miss(Vector2 _pos)
    {
        GameObject Bullet = Instantiate(Resources.Load<GameObject>("shot_miss"));
        Bullet.transform.position = _pos;

        Destroy(this.gameObject);
    }

    private IEnumerator DoMiss()
    {
        yield return new WaitForSeconds(0.5f);
        Miss(transform.position);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //StopAllCoroutines();
        StopCoroutine(DoDestroy());
        if(!Glancing) DestroyDamage();
    }

    private void OnTriggerEnter2D(Collider2D _col)
    {
        //  Don't let the bullet hit multiple components before it destroys its self
        if (HitActivated) return;

        Debug.Log("Hit");
        HitActivated = true;
        HitCol = _col;
        StartCoroutine(DoDestroy());
    }

    public IEnumerator DoDestroy()
    {
        yield return new WaitForSeconds(0.2f);
        DestroyDamage();
    }

    public void Destroy()
    {
        //  Spawn the projectile hit sprite
        GameObject Hit;
        Hit = Instantiate(Resources.Load<GameObject>("AutoCannon_hit_FX"));
        Hit.transform.position = transform.position;
        Destroy(this.gameObject);
    }

    public void DestroyDamage()
    {
        //  Get the damage parameters 
        Tank HitTank = HitCol.transform.GetComponentInParent<Tank>();
        float Damage = Firedby.G1_ap;
        string Calibre = Firedby.G1_gun_calibre;
        string ArmourClass = HitCol.transform.GetComponentInParent<Tank>().Attr.Weight_class;

        Debug.Log(HitCol);

        //  Spawn the correct projectile hit sprite
        GameObject Hit;

        if (HitCol.name == "Front_Armour")
        {
            Hit = Instantiate(Resources.Load<GameObject>("AutoCannon_hit_FX"));
            HitTank.Damage(Tank.HealthType.PRIMARY, Damage, Calibre, ArmourClass, true);
            HitTank.Damage(Tank.HealthType.TRACK, Damage, Calibre, ArmourClass, false);
        }
        else if (HitCol.transform.name == "Side_Armour" && !Glancing)
        {
            Hit = Instantiate(Resources.Load<GameObject>("AutoCannon_hit_FX"));
            HitTank.Damage(Tank.HealthType.PRIMARY, Firedby.G1_ap, Firedby.G1_gun_calibre, ArmourClass, true);
            HitTank.Damage(Tank.HealthType.ENGINE, Damage, Calibre, ArmourClass, false);
            HitTank.Damage(Tank.HealthType.AMMO_RACK, Damage, Calibre, ArmourClass, false);
        }
        else if (HitCol.name == "Rear_Armour")
        {
            Hit = Instantiate(Resources.Load<GameObject>("AutoCannon_hit_FX"));
            HitTank.Damage(Tank.HealthType.PRIMARY, Firedby.G1_ap, Firedby.G1_gun_calibre, ArmourClass, true);
            HitTank.Damage(Tank.HealthType.ENGINE, Damage, Calibre, ArmourClass, false);
        }
        else
        {
            Hit = Instantiate(Resources.Load<GameObject>("hit_sparks_bigger"));

            //  Reflect the projectile
            Vector2 pos = transform.position;
            Vector2 vel = GetComponent<Rigidbody2D>().velocity;
            vel = Vector2.Reflect(vel, Vector2.up);
            GetComponent<Rigidbody2D>().velocity = vel;
            transform.rotation = Quaternion.Euler(0, 0, Mathf.Rad2Deg * Mathf.Atan2((pos.y + vel.y) - pos.y, (pos.x + vel.x) - pos.x));

            //  Don't destroy normally, delay destroy as miss
            StartCoroutine(DoMiss());
            Hit.transform.position = transform.position;
            GetComponent<Collider2D>().enabled = false;

            return;
        }

        Hit.transform.position = transform.position;
        Destroy(this.gameObject);
    }

    public void Instantiate(Tank _firedBy, bool _glancing)
    {
        Firedby = _firedBy.Attr;

        //  Ignore collisions with the fired by object
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), _firedBy.transform.Find("Front_Armour").GetComponent<Collider2D>());
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), _firedBy.transform.Find("Side_Armour").GetComponent<Collider2D>());
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), _firedBy.transform.Find("Rear_Armour").GetComponent<Collider2D>());
        Glancing = _glancing;
    }

    public void Instantiate(AITank _firedBy, bool _glancing)
    {
        Firedby = _firedBy.Attr;

        //  Ignore collisions with the fired by object
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), _firedBy.transform.Find("Projectile Collider").GetComponent<Collider2D>());
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), _firedBy.GetComponent<Collider2D>());

        Glancing = _glancing;
    }

    public void SetTarget(Vector2 _pos)
    {
        TargetPosition = _pos;
    }
}
