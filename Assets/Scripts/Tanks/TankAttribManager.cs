﻿using ExcelDataReader;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEngine;

public class TankAttribManager : MonoBehaviour
{
    //  Singleton instance
    private static TankAttribManager _instance;
    public static TankAttribManager Instance
    {
        get
        {
            //  If there is already an instance, return it
            if (_instance != null)
            {
                return _instance;
            }
            //  Otherwise, create an instance of AttribImport on an empty game object if the instance doesn't already exist
            else
            {
                _instance = new GameObject("Tank Attributes").AddComponent<TankAttribManager>();
                _instance.LoadAttr();
                return _instance;
            }
        }
    }

    //  Make the dictionary serializable for inspector debugging
    [System.Serializable]
    public class SDict : SerializableDictionary<string, TankAttrib> { }
    public SDict Attribs = new SDict();

    //  Loads the tank attributes from the balancing file
    private void LoadAttr()
    {
        //  Get the file location, only works while running in editor
        string fileLoc = Application.dataPath + "/StreamingAssets/Balancing/" + "Balancing.xlsx";

        //  Open and read the excel file
        using (var stream = File.Open(fileLoc, FileMode.Open, FileAccess.Read))
        {
            using (var reader = ExcelReaderFactory.CreateReader(stream))
            {
                DataSet result = reader.AsDataSet();

                foreach (DataTable t in result.Tables)
                {
                    if (!t.TableName.Contains("Val_")) continue;

                    for (int i = 2; i < t.Columns.Count; i++)
                    {
                        //  The current tank name being pulled
                        string tankName = t.Rows[0][i].ToString();

                        //  If the tank name cell is blank, don't look for values
                        if (tankName == "") continue;

                        //  The current tank attribute set
                        TankAttrib attr = new TankAttrib();

                        // Armour class will always be the last in the enum
                        if (t.TableName.Contains("Val_LT")) TankAttrib.SetAttrib(attr, Enum.GetNames(typeof(AttribType)).Length - 1, "LT");
                        else if (t.TableName.Contains("Val_MT")) TankAttrib.SetAttrib(attr, Enum.GetNames(typeof(AttribType)).Length - 1, "MT");
                        else if (t.TableName.Contains("Val_HT")) TankAttrib.SetAttrib(attr, Enum.GetNames(typeof(AttribType)).Length - 1, "HT");
                        else if (t.TableName.Contains("Val_TD")) TankAttrib.SetAttrib(attr, Enum.GetNames(typeof(AttribType)).Length - 1, "TD");

                        for (int j = 1; j < t.Rows.Count; j++)
                        {
                            //  The current attribute being pulled
                            string curVal = t.Rows[j][i].ToString();

                            //  Set the current attribute in the attr var
                            if(curVal != "")
                            {
                                TankAttrib.SetAttrib(attr, j - 1, curVal);
                            }
                            //  If the value is blank, set it to zero
                            else
                            {
                                TankAttrib.SetAttrib(attr, j - 1, "0");
                            }
                        }

                        Attribs.Add(tankName, attr);
                    }
                }
            }
        }
    }
}