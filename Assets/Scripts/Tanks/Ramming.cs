﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ramming : MonoBehaviour
{
    private Tank T;

    void Start()
    {
        T = GetComponentInParent<Tank>();
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), transform.parent.Find("driving").GetComponent<Collider2D>());
        GetComponent<Collider2D>().enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.tag != "Tank")
            return;

        Tank OT = _col.GetComponentInParent<Tank>();

        if (T.Attr.Mass >= OT.Attr.Mass)
        {
            if (T.Attr.Mass - OT.Attr.Mass != 0)
            {
                OT.ChangeHP(Tank.HealthType.PRIMARY, -(T.Attr.Mass - OT.Attr.Mass), true);
                OT.ChangeHP(Tank.HealthType.AMMO_RACK, -(T.Attr.Mass - OT.Attr.Mass), false);
                OT.ChangeHP(Tank.HealthType.ENGINE, -(T.Attr.Mass - OT.Attr.Mass), false);
                OT.ChangeHP(Tank.HealthType.TRACK, -(T.Attr.Mass - OT.Attr.Mass), false);
            }
            else
            {
                OT.ChangeHP(Tank.HealthType.PRIMARY, 0, true, Tank.DamageType.SPALLING);
            }
        }
    }
}
