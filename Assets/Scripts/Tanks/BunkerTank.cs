﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BunkerTank : AITank
{
    public float ShotDelay = 3f;
    public float ShotCooldown = 3f;

    private float LastShotTime = -999f;

    //  Tanks that have entered the trigger and the time they entered
    public Dictionary<Tank, float> TanksInRange = new Dictionary<Tank, float>();

    // Start is called before the first frame update
    internal override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    internal override void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            //Anims.PlayGun1Anim();
            //Anims.PlayFiringSmoke();
            //Shoot(transform);
        }

        if (Time.time < LastShotTime + ShotCooldown)
            return;

        //  Order the list to prioritize the closest non-player tank and remove friendlies from the list
        var OrderedTanks = TanksInRange.
            Where(T => T.Key.Direction != Direction).
            OrderBy(T => T.Key.ActivePlayer ? 1 : 0).
            ThenBy(T => Vector2.Distance(transform.position, T.Key.transform.position));

        int i = 0;
        foreach(var t in OrderedTanks)
        {
            if (Time.time > t.Value + ShotDelay)
            {
                Shoot(t.Key.transform.position);
                LastShotTime = Time.time;
                break;
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.tag != "Tank")
            return;

        if (!TanksInRange.ContainsKey(_col.GetComponentInParent<Tank>()))
        {
            TanksInRange.Add(_col.GetComponentInParent<Tank>(), Time.time);
        }
    }

    public void OnTriggerExit2D(Collider2D _col)
    {
        if(_col.tag == "Tank")
            TanksInRange.Remove(_col.GetComponentInParent<Tank>());
    }
}
