﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

[System.Serializable]
public class FiringArcSettings
{
    public Vector2 SPoint1;
    public float Angle1;
    public float Dist1;

    [Header("")]
    public Vector2 SPoint2;
    public float Angle2;
    public float Dist2;

    [Header("")]
    public float ZDepth = -10f;
    public float DecoOffsetX = 0.01f;
    public Vector3 TotalOffset = Vector2.zero;
}

public class FiringArc : MonoBehaviour
{
    public LineRenderer RendLine1;
    public LineRenderer RendLine2;
    public LineRenderer RendDeco1;
    public LineRenderer RendDeco2;
    public LineRenderer RendDeco3;
    public LineRenderer RendDeco4;

    [HideInInspector]
    public Vector2[] FirePoly;

    public static FiringArc Instance;

    public void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //  Don't try to position if there's no current player tank
        if (Tank.ActivePlayerTank == null)
        {
            SetEnabled(false);
            return;
        }

        Tank T = Tank.ActivePlayerTank;

        Vector2 EPoint1;
        EPoint1 = MathK.LineFromAngle(T.FAS.SPoint1, T.FAS.Angle1, T.FAS.Dist1).EndPoint;

        Vector2 EPoint2;
        EPoint2 = MathK.LineFromAngle(T.FAS.SPoint2, T.FAS.Angle2, T.FAS.Dist2).EndPoint;

        //  Draw the first line
        RendLine1.SetPosition(0, new Vector3(T.FAS.SPoint1.x, T.FAS.SPoint1.y, T.FAS.ZDepth).RoundPos());
        RendLine1.SetPosition(1, new Vector3(EPoint1.x, EPoint1.y, T.FAS.ZDepth).RoundPos());

        //  Draw the connecting line
        RendDeco1.SetPosition(0, new Vector3(T.FAS.SPoint1.x, T.FAS.SPoint1.y, T.FAS.ZDepth).RoundPos());
        RendDeco1.SetPosition(1, new Vector3(T.FAS.SPoint2.x, T.FAS.SPoint2.y, T.FAS.ZDepth).RoundPos());

        //  Draw the second line
        RendLine2.SetPosition(0, new Vector3(T.FAS.SPoint2.x, T.FAS.SPoint2.y, T.FAS.ZDepth).RoundPos());
        RendLine2.SetPosition(1, new Vector3(EPoint2.x, EPoint2.y, T.FAS.ZDepth).RoundPos());

        //  Draw the decoration lines beheind the actual functional lines
        RendDeco2.SetPosition(0, new Vector3(T.FAS.SPoint1.x, T.FAS.SPoint1.y, T.FAS.ZDepth).RoundPos());
        RendDeco2.SetPosition(1, new Vector3(T.FAS.SPoint1.x + T.FAS.DecoOffsetX, T.FAS.SPoint1.y, T.FAS.ZDepth).RoundPos());

        RendDeco3.SetPosition(0, new Vector3(T.FAS.SPoint2.x, T.FAS.SPoint2.y, T.FAS.ZDepth).RoundPos());
        RendDeco3.SetPosition(1, new Vector3(T.FAS.SPoint2.x + T.FAS.DecoOffsetX, T.FAS.SPoint2.y, T.FAS.ZDepth).RoundPos());

        RendDeco4.SetPosition(0, new Vector3(T.FAS.SPoint1.x + T.FAS.DecoOffsetX, T.FAS.SPoint1.y, T.FAS.ZDepth).RoundPos());
        RendDeco4.SetPosition(1, new Vector3(T.FAS.SPoint2.x + T.FAS.DecoOffsetX, T.FAS.SPoint2.y, T.FAS.ZDepth).RoundPos());

        //  Follow the tank
        Vector3 TPos = (T.FAS.TotalOffset + Tank.ActivePlayerTank.transform.position).RoundPos();
        transform.position = TPos;

        //  Build the polygon collider used for checking the cursor position while firing
        List<Vector2> Points = new List<Vector2>
        {
            transform.TransformPoint(MathK.LineFromAngle(T.FAS.SPoint1, T.FAS.Angle1, T.FAS.Dist1 * 10).EndPoint),
            transform.TransformPoint(T.FAS.SPoint1),
            transform.TransformPoint(T.FAS.SPoint2),
            transform.TransformPoint(MathK.LineFromAngle(T.FAS.SPoint2, T.FAS.Angle2, T.FAS.Dist2 * 10).EndPoint)
        };

        FirePoly = Points.ToArray();
    }

    public void SetEnabled(bool _enabled)
    {
        RendDeco1.enabled = _enabled;
        RendDeco2.enabled = _enabled;
        RendDeco3.enabled = _enabled;
        RendDeco4.enabled = _enabled;
        RendLine1.enabled = _enabled;
        RendLine2.enabled = _enabled;
    }
}
