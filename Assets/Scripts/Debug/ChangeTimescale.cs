﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTimescale : MonoBehaviour
{
    public float TimeScale = 1;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.PageUp))
        {
            this.TimeScale *= 2;
            Time.timeScale = TimeScale;
        }

        if (Input.GetKeyDown(KeyCode.PageDown))
        {
            this.TimeScale /= 2;
            Time.timeScale = TimeScale;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 100, 100), TimeScale.ToString());
    }
}
