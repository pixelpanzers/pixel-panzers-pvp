﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

[RequireComponent(typeof(LineRenderer))]
public class AngleTest : MonoBehaviour
{
    LineRenderer LR;
    LineRenderer LR2;
    LineRenderer LR3;
    float LineAngle = 0;

    public float MaxAngle = 180f;
    public float MinAngle = -180f;

    public bool Dbg = false;

    private void Start()
    {
        GameObject Line = new GameObject("Line");
        LR = Line.AddComponent<LineRenderer>();
        LR.startWidth = 0.05f;
        LR.endWidth = 0.05f;
    }

    // Update is called once per frame
    void Update ()
    {
        Vector3 MousePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 SPoint = new Vector3(transform.position.x, transform.position.y, -1);
        Vector3 EPoint = new Vector3(MousePoint.x, MousePoint.y, -1);
        Vector3 ConPointMax = MathK.ConstrainToLine(SPoint, MathK.LineFromAngle(transform.position, MaxAngle).EndPoint, EPoint);
        LineAngle = Mathf.Rad2Deg * Mathf.Atan2(EPoint.y - SPoint.y, EPoint.x - SPoint.x) + 180;

        //  Line to mouse cursor
        LR.SetPosition(0, SPoint);

        //  If the angle between this game object and the cursor is not within the specified bounds, clamp the shot point
        if (LineAngle > MaxAngle)
            LR.SetPosition(1, MathK.ConstrainToLine(SPoint, MathK.LineFromAngle(transform.position, MaxAngle).EndPoint, EPoint));
        else if (LineAngle < MinAngle)
            LR.SetPosition(1, MathK.ConstrainToLine(SPoint, MathK.LineFromAngle(transform.position, MinAngle).EndPoint, EPoint));
        else
            LR.SetPosition(1, EPoint);
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 100, 100), "Angle: " + LineAngle);
    }
}

