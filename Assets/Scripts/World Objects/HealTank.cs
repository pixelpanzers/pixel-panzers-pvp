﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HealTank : MonoBehaviour
{
    //  Tank, last heal time
    public static Dictionary<Tank, float> TankHealTimes = new Dictionary<Tank, float>();
    public static Dictionary<Tank, float> TankHealStartTimes = new Dictionary<Tank, float>();

    public float Delay = 8f;
    public float Cooldown = 20f;
    public float HP = 100;

    void Update()
    {
        //  Remove tanks which are no-longer on cooldown
        foreach(Tank t in TankHealTimes.Keys)
        {
            //if(Time.timeSinceLevelLoad >= TankHealTimes[t] + Delay)
        }

        for (int i = TankHealTimes.Count - 1; i > -1; i--)
        {
            Tank t = TankHealTimes.Keys.ToList()[i];

            if (Time.timeSinceLevelLoad >= TankHealTimes[TankHealTimes.Keys.ToList()[i]] + Cooldown)
            {
                //  Remove the tank from the cooldown list
                TankHealTimes.Remove(t);

                //  Reset the heal timer if necessary
                if(TankHealStartTimes.ContainsKey(t))
                    TankHealStartTimes[t] = Time.timeSinceLevelLoad;

                Debug.Log("Cooldown removed");
            }
        }

        foreach(Tank t in TankHealStartTimes.Keys)
        {
            //  Don't heal the tank if it is in cooldown
            if (!TankHealTimes.ContainsKey(t))
            {

                //  Heal the tank after it has been in the zone for x ammount of time
                if (Time.timeSinceLevelLoad >= TankHealStartTimes[t] + Delay)
                {
                    t.ChangeHP(Tank.HealthType.PRIMARY, HP, true, Tank.DamageType.SPALLING);
                    TankHealTimes.Add(t, Time.timeSinceLevelLoad);
                    Debug.Log("Healed");
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D _col)
    {
        if (!TankHealStartTimes.ContainsKey(_col.GetComponentInParent<Tank>()))
            TankHealStartTimes.Add(_col.GetComponentInParent<Tank>(), Time.timeSinceLevelLoad);

        Debug.Log("Entered");
    }

    private void OnTriggerExit2D(Collider2D _col)
    {
        TankHealStartTimes.Remove(_col.GetComponentInParent<Tank>());
        Debug.Log("Exited");
    }
}
