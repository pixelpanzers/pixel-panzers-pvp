﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public enum CaptureState
{
    NONE,
    LEFT_CAPTURING,
    LEFT_CAPTURED,
    RIGHT_CAPTURING,
    RIGHT_CAPTURED
}

public enum CapturedBy
{
    NONE,
    LEFT,
    RIGHT
}

public class CapturePoint : MonoBehaviour
{
    public float TarCaptureTime = 10f;
    public char PointID;

    private List<Tank> CapturingTanks = new List<Tank>();

    private float LeftCapTime, RightCapTime;

    public CapturedBy CappedBy;

    private CaptureState state;
    public CaptureState State
    {
        get => state;
        set 
        {
            state = value;
            UIUpdate();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (CapturingTanks.Count == 0)
        {
            LeftCapTime = 0;
            RightCapTime = 0;
            State = CaptureState.NONE;
            return;
        }

        //  Get the total number of right/left facing tanks within the capture point
        int NumRightTanks = CapturingTanks.Count(T => T.Direction == Tank.TankDir.RIGHT_FACING);
        int NumLeftTanks = CapturingTanks.Count - NumRightTanks;

        if(NumRightTanks > NumLeftTanks)
        {
            if (State == CaptureState.RIGHT_CAPTURED || CappedBy == CapturedBy.RIGHT)
                return;

            if (RightCapTime > TarCaptureTime)
            {
                State = CaptureState.RIGHT_CAPTURED;
                CappedBy = CapturedBy.RIGHT;
                return;
            }

            LeftCapTime = 0;
            RightCapTime += Time.deltaTime;

            State = CaptureState.RIGHT_CAPTURING;
        }
        else if(NumLeftTanks > NumRightTanks)
        {
            if (State == CaptureState.LEFT_CAPTURED || CappedBy == CapturedBy.LEFT)
                return;

            if (LeftCapTime > TarCaptureTime)
            {
                State = CaptureState.LEFT_CAPTURED;
                CappedBy = CapturedBy.LEFT;
                return;
            }

            RightCapTime = 0;
            LeftCapTime += Time.deltaTime;

            State = CaptureState.LEFT_CAPTURING;
        }
    }

    public void UIUpdate()
    {
        if (State == CaptureState.LEFT_CAPTURED)
        {
            UIRef.CapUISets[PointID].LeftCaptured.SetActive(true);
            UIRef.CapUISets[PointID].RightCaptured.SetActive(false);
            UIRef.CapUISets[PointID].LeftCapturing.SetActive(false);
            UIRef.CapUISets[PointID].RightCapturing.SetActive(false);
        }
        else if (State == CaptureState.RIGHT_CAPTURED)
        {
            UIRef.CapUISets[PointID].LeftCaptured.SetActive(false);
            UIRef.CapUISets[PointID].RightCaptured.SetActive(true);
            UIRef.CapUISets[PointID].LeftCapturing.SetActive(false);
            UIRef.CapUISets[PointID].RightCapturing.SetActive(false);
        }
        else if (State == CaptureState.LEFT_CAPTURING)
        {
            UIRef.CapUISets[PointID].LeftCaptured.SetActive(false);
            UIRef.CapUISets[PointID].RightCaptured.SetActive(false);
            UIRef.CapUISets[PointID].LeftCapturing.SetActive(true);
            UIRef.CapUISets[PointID].RightCapturing.SetActive(false);
        }
        else if (State == CaptureState.RIGHT_CAPTURING)
        {
            UIRef.CapUISets[PointID].LeftCaptured.SetActive(false);
            UIRef.CapUISets[PointID].RightCaptured.SetActive(false);
            UIRef.CapUISets[PointID].LeftCapturing.SetActive(false);
            UIRef.CapUISets[PointID].RightCapturing.SetActive(true);
        }
        else // No state
        {
            UIRef.CapUISets[PointID].LeftCapturing.SetActive(false);
            UIRef.CapUISets[PointID].RightCapturing.SetActive(false);

            if (CappedBy == CapturedBy.NONE)
            {
                UIRef.CapUISets[PointID].LeftCaptured.SetActive(false);
                UIRef.CapUISets[PointID].RightCaptured.SetActive(false);
            }
            else if (CappedBy == CapturedBy.LEFT)
            {
                UIRef.CapUISets[PointID].LeftCaptured.SetActive(true);
                UIRef.CapUISets[PointID].RightCaptured.SetActive(false);
            }
            else if (CappedBy == CapturedBy.RIGHT)
            {
                UIRef.CapUISets[PointID].LeftCaptured.SetActive(false);
                UIRef.CapUISets[PointID].RightCaptured.SetActive(true);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D _col)
    {
        if (!CapturingTanks.Contains(_col.GetComponentInParent<Tank>()))
            CapturingTanks.Add(_col.GetComponentInParent<Tank>());

        Debug.Log("Entered");
    }

    private void OnTriggerExit2D(Collider2D _col)
    {
        CapturingTanks.Remove(_col.GetComponentInParent<Tank>());
        Debug.Log("Exited");
    }
}
