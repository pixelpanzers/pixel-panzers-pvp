﻿using UnityEngine;
using System.Linq;
using System;
using UnityEngine.U2D;

namespace Utils
{
    public static class MathK
    {
        public static Vector3 RoundPos(this Vector3 _vec)
        {
            Vector3 result;
            result.x = (Mathf.Round(_vec.x * 100) / 100);
            result.y = (Mathf.Round(_vec.y * 100) / 100);
            result.z = (Mathf.Round(_vec.z * 100) / 100);

            return result;
        }

        public static Vector2 RoundPos(this Vector2 _vec)
        {
            Vector2 result;
            result.x = (Mathf.Round(_vec.x * 100) / 100);
            result.y = (Mathf.Round(_vec.y * 100) / 100);

            return result;
        }

        public static Vector3 Clamp2D(this Vector3 _vec, float _maxX, float _minX, float _maxY, float _minY)
        {
            Vector2 Clamped = _vec;
            Clamped.x = Mathf.Clamp(_vec.x, _minX, _maxX);
            Clamped.y = Mathf.Clamp(_vec.y, _minY, _maxY);

            return Clamped;
        }

        public static Vector2 Clamp2D(this Vector2 _vec, float _maxX, float _minX, float _maxY, float _minY)
        {
            Vector2 Clamped = _vec;
            Clamped.x = Mathf.Clamp(_vec.x, _minX, _maxX);
            Clamped.y = Mathf.Clamp(_vec.y, _minY, _maxY);

            return Clamped;
        }

        public static Line LineFromAngle(Vector2 _startPoint, float _a, float _length = 10000f)
        {
            float aRad = Mathf.Deg2Rad * _a;
            float x1 = _startPoint.x;
            float y1 = _startPoint.y;
            float x2 = x1 + Mathf.Cos(aRad) * _length;
            float y2 = y1 + Mathf.Sin(aRad) * _length;

            return new Line(new Vector2(x1, y1), new Vector2(x2, y2));
        }

        public static Vector2 ConstrainToLine(Line _l, Vector2 _p)
        {
            return ConstrainToLine(_l.StartPoint, _l.EndPoint, _p);
        }

        public static Vector2 ConstrainToLine(Vector2 _a, Vector2 _b, Vector2 _P)
        {
            Vector2 ap = _P - _a;
            Vector2 ab = _b - _a;

            float magnitudeAB = ab.SqrMagnitude();
            float ABAPproduct = Vector2.Dot(ap, ab);
            float distance = ABAPproduct / magnitudeAB;

            if (distance < 0)
            {
                return _a;

            }
            else if (distance > 1)
            {
                return _b;
            }
            else
            {
                return _a + ab * distance;
            }
        }

        public static bool IsInPolygon(this Vector2 testPoint, Vector2[] vertices)
        {
            if (vertices.Count() < 3) return false;
            bool isInPolygon = false;
            var lastVertex = vertices[vertices.Count() - 1];
            foreach (var vertex in vertices)
            {
                if (testPoint.y.IsBetween(lastVertex.y, vertex.y))
                {
                    double t = (testPoint.y - lastVertex.y) / (vertex.y - lastVertex.y);
                    double x = t * (vertex.x - lastVertex.x) + lastVertex.x;
                    if (x >= testPoint.x) isInPolygon = !isInPolygon;
                }
                else
                {
                    if (testPoint.y == lastVertex.y && testPoint.x < lastVertex.x && vertex.y > testPoint.y) isInPolygon = !isInPolygon;
                    if (testPoint.y == vertex.y && testPoint.x < vertex.x && lastVertex.y > testPoint.y) isInPolygon = !isInPolygon;
                }

                lastVertex = vertex;
            }

            return isInPolygon;
        }

        public static bool IsBetween(this float x, float a, float b)
        {
            return (x - a) * (x - b) < 0;
        }
    }

    public class Line
    {
        private Vector2 startPoint;
        private Vector2 endPoint;

        public Line(Vector2 _s, Vector2 _e)
        {
            StartPoint = _s;
            EndPoint = _e;
        }

        public Vector2 StartPoint
        {
            get
            {
                return startPoint;
            }

            set
            {
                startPoint = value;
            }
        }

        public Vector2 EndPoint
        {
            get
            {
                return endPoint;
            }

            set
            {
                endPoint = value;
            }
        }
    }
}